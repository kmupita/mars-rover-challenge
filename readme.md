# Synatic- Kudzai Mupita Mars Rover Challenge

## How to run this file

Open `index.html` in Google Chrome. Open the console.

In the console, call the `roverCommandList` function passing a string of commands using the commands below. Eg: `roverCommandList('rfrf')`.

## Commands

Commands to move the rover are:

- `l` = turns the robot to the left.
- `r` = turns the robot to the right.
- `f` = moves the robot forward.
- `b` = moves the robot backward.

## View travel data

- `getTravelData()` = print out an array log of the rover's steps.
